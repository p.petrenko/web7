// Функция внутри $( document ).ready() срабатывает после загрузки DOM.
$(document).ready(function() {
    $(".slick-example").slick({
        infinite: true,
        dots: true,
        dotsClass: "my-dots",
        //dotsClass: 'my-dots-class',
        prevArrow: $("#prev"),
        nextArrow: $("#next"),
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    });
});